#!/usr/bin/env python

# MIT License
# Copyright (c) 2019 JetsonHacks
# See license
# Using a CSI camera (such as the Raspberry Pi Version 2) connected to a
# NVIDIA Jetson Nano Developer Kit using OpenCV
# Drivers for the camera and OpenCV are included in the base image
from __future__ import print_function
import roslib
roslib.load_manifest('jetson_csi_camera')
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


import sys
# gstreamer_pipeline returns a GStreamer pipeline for capturing from the CSI camera
# Defaults to 1280x720 @ 60fps
# Flip the image by setting the flip_method (most common values: 0 and 2)
# display_width and display_height determine the size of the window on the screen


def gstreamer_pipeline(
    capture_width=1280,
    capture_height=720,
    display_width=640,
    display_height=360,
    framerate=60,
    flip_method=0,
):
    return (
        "nvarguscamerasrc ! "
        "video/x-raw(memory:NVMM), "
        "width=(int)%d, height=(int)%d, "
        "format=(string)NV12, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )


def show_camera():
    global cap
    framerate = 30
    # To flip the image, modify the flip_method parameter (0 and 2 are the most common)
    print(gstreamer_pipeline(framerate=framerate, flip_method=0)) 
    cap = cv2.VideoCapture(gstreamer_pipeline(flip_method=0, framerate=framerate), cv2.CAP_GSTREAMER)
    
    if cap.isOpened():
        pub = rospy.Publisher('/camera/image_raw', Image , queue_size=1)
        rospy.init_node('jetson_csi_camera', anonymous=True)
        rate = rospy.Rate(framerate) # hz
        bridge = CvBridge()
        while not rospy.is_shutdown():
            ret_val, img = cap.read()
            image_message = bridge.cv2_to_imgmsg(img, encoding="bgr8")
            pub.publish(image_message)
            cv2.waitKey(3) & 0xFF
            rate.sleep()
    else:
        ros("Unable to open camera")

if __name__ == '__main__':
    try:
        show_camera()
    except rospy.ROSInterruptException:
        pass
    finally:
        cap.release()
        cv2.destroyAllWindows()
